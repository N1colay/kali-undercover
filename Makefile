dir = usr/share/kali-undercover/
TAROPTS := --sort=name --format ustar

.PHONY: build clean install

build:
	cd kali-undercover-profile && tar $(TAROPTS) -cvjf ../kali-undercover-profile.tar.bz2 *

clean:
	rm -f kali-undercover-profile.tar.bz2

install:
	install -d $(DESTDIR)/$(dir)
	install -m 644 kali-undercover-profile.tar.bz2 $(DESTDIR)/$(dir)

